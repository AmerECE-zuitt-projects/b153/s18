let trainer = {};

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = [
    "Pickachu",
    "Charizard",
    "Squirtle",
    "Bulbasaur"
];
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
};
trainer.talk = function () {
    console.log(`"${this.pokemon[0]}! I Choose You!"`)
};
console.log(trainer);
console.log(trainer.name);
console.log(trainer.talk());