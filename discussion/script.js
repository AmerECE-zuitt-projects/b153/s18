/* Objects
    - an object is a data type that is used to represent the specific details about a single concept.
    - objects are created via curly braces {} and key-value pairs, separated by a colon.
    - to the right of the colon is the value
    - when using multiple key-value pairs, separate each one with a coma.
    - objects can contain other objects, arrays and even functions.
*/

let person = {
    firstName: "John",
    lastName: "Smith",
    location: {
        city: "Tokyo",
        country: "Japan"
    },
    emails: ["john@mail.com", "johnsmith@mail.xyz"],
    brushTeeth: function () {
        console.log(`${this.firstName} has brushed his teeth`)
    }
};

/* Accessing Object Properties
    - use the dot notation to access an object's property's value
*/
console.log(person.lastName);
console.log(person.emails[0]);
console.log(person.location.country);
console.log(person.brushTeeth());

// Add and Editing Object Properties
let car = {
    manufactureDate: 1999
};
car.name = "Honda Civic";
car.manufactureDate = 2019;
console.log(car);

let example = {
    prop1: [
        {
            prop2: ["cat", "dog"]
        }
    ]
};
console.log(example.prop1[0].prop2[1]);